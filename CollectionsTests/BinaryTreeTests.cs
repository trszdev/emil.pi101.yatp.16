﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Collections;
using System.Linq;
using System.Collections.Generic;

namespace CollectionsTests
{
    [TestClass]
    public class BinaryTreeTests
    {
        readonly Random Random = new Random();

        [TestMethod]
        public void Test_AddContains_Common()
        {
            var tree = new BinaryTree<int>();
            tree.Add(1);
            tree.Add(3);
            tree.Add(2);
            Assert.IsTrue(tree.Contains(1));
            Assert.IsTrue(tree.Contains(2));
            Assert.IsTrue(tree.Contains(3));
        }

        [TestMethod]
        public void Test_AddContains_SameElement1()
        {
            var tree = new BinaryTree<int>();
            tree.Add(1);
            tree.Add(1);
            tree.Add(1);
            tree.Add(1);
            Assert.IsTrue(tree.Contains(1));
            Assert.IsFalse(tree.Contains(0));
        }

        [TestMethod]
        public void Test_AddContains_SameElement2()
        {
            var tree = new BinaryTree<int>();
            tree.Add(1);
            tree.Add(1);
            tree.Add(1);
            Assert.IsTrue(tree.Contains(1));
            Assert.IsFalse(tree.Contains(0));
        }


        [TestMethod]
        public void Test_AddContains_BigChain10k()
        {
            var tree = new BinaryTree<int>(Enumerable.Range(0, 10000));
            foreach (var elem in Enumerable.Range(0, 10000))
                Assert.IsTrue(tree.Contains(elem));
        }


        [TestMethod]
        public void Test_AddContains_BigRandom100k()
        {
            var values = new HashSet<int>();
            var tree = new BinaryTree<int>();
            for(var i=0;i<100000;i++){
                var value = Random.Next(-10000, 10000);
                values.Add(value);
                tree.Add(value);
            }

            foreach (var value in values)
                Assert.IsTrue(tree.Contains(value));
        }


        [TestMethod]
        public void Test_Enumerate_Common()
        {
            var tree = new BinaryTree<int>(new[] { 1, 2, 1, 1, 5, 1, 6, 8, 2, 3, 7 });
            var t = tree.ToArray();
            Assert.IsTrue(tree.SequenceEqual(new[] { 1, 1, 1, 1, 2, 2, 3, 5, 6, 7, 8 }));
        }

        [TestMethod]
        public void Test_Enumerate_Common2()
        {
            var tree = new BinaryTree<int>(new[] { 1, 2, 3, 3, 3, 3, 3, 1, 1, 2, 0 });
            var t = tree.ToArray();
            Assert.IsTrue(tree.SequenceEqual(new[] { 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3 }));
        }

        [TestMethod]
        public void Test_Enumerate_Empty()
        {
            var tree = new BinaryTree<int>();
            Assert.IsTrue(tree.SequenceEqual(new int[0]));
        }


        [TestMethod]
        public void Test_Index_Common1()
        {
            var tree = new BinaryTree<int>(new[] { 1, 2, 3, 3, 3, 3, 3, 1, 1, 2, 0 });
            var expected = new[] { 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3 };
            for (var i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], tree[i]);
        }


        [TestMethod]
        public void Test_Index_Common2()
        {
            var tree = new BinaryTree<int>(new[] { 1, 4, 3, 2, 1, 10, -10, 10 });
            var expected = new [] {-10, 1, 1, 2, 3, 4, 10, 10 };
            for (var i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], tree[i]);
        }


        [TestMethod]
        public void Test_Index_BadIndex1()
        {
            var tree = new BinaryTree<int>();
            try
            {
                var a = tree[0];
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void Test_Index_BadIndex2()
        {
            var tree = new BinaryTree<int>(new[] { 1, 2, 3, 3, 3, 3, 3, 1, 1, 2, 0 });
            try
            {
                var a = tree[11];
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void Test_Enumerate_Single()
        {
            var tree = new BinaryTree<int>();
            tree.Add(1);
            Assert.IsTrue(tree.SequenceEqual(new [] {1}));
        }

        [TestMethod]
        public void Test_Enumerate_Double()
        {
            var tree = new BinaryTree<int>();
            tree.Add(2);
            tree.Add(1);
            Assert.IsTrue(tree.SequenceEqual(new[] { 1, 2 }));
        }


        [TestMethod]
        public void Test_Enumerate_AlreadySorted1()
        {
            var tree = new BinaryTree<int>(new[] { 1, 1, 2, 2, 3, 5, 6, 7 });
            Assert.IsTrue(tree.SequenceEqual(new[] { 1, 1, 2, 2, 3, 5, 6, 7 }));
        }


        [TestMethod]
        public void Test_Enumerate_AlreadySorted2()
        {
            var tree = new BinaryTree<int>(new[] { 7, 6, 5, 3, 2, 2, 1, 1 });
            Assert.IsTrue(tree.SequenceEqual(new[] { 1, 1, 2, 2, 3, 5, 6, 7 }));
        }


        [TestMethod]
        public void Test_Contains_Empty()
        {
            var tree = new BinaryTree<int>();
            Assert.IsFalse(tree.Contains(1));
            Assert.IsFalse(tree.Contains(0));
        }

    }
}
