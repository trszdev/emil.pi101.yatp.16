﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    public class BinaryTree<T> : IEnumerable<T> 
        where T : IComparable<T>
    {
        TreeNode<T> Root;

        public BinaryTree(IEnumerable<T> collection)
        {
            foreach (var elem in collection) Add(elem);
        }

        public BinaryTree()
        {
        }

        public void Add(T key){
            if (Root == null)
            {
                Root = new TreeNode<T>(key);
                return;
            }
            var current = Root;
            for (; ; )
            {
                current.Weight++;
                if (key.CompareTo(current.Value) >= 0)
                {
                    if (current.Right == null)
                    {
                        current.Right = new TreeNode<T>(key);
                        break;
                    }
                    else current = current.Right;
                }
                else
                {
                    if (current.Left == null)
                    {
                        current.Left = new TreeNode<T>(key);
                        break;
                    }
                    else current = current.Left;
                }
            }
        }

        public bool Contains(T key){
            var current = Root;
            for(;;)
            {
                if (current == null) return false;
                var comparison = key.CompareTo(current.Value);
                if (comparison == 0) return true;
                if (comparison >= 1) current = current.Right;
                else current = current.Left;
            }
        }

       
        public IEnumerator<T> GetEnumerator()
        {
            if (Root == null) yield break;
            var stack = new Stack<TreeNode<T>>();
            var current = Root;
            while (stack.Count != 0 || current != null)
            {
                while (current != null)
                {
                    stack.Push(current);
                    current = current.Left;
                }
                current = stack.Pop();
                yield return current.Value;
                current = current.Right;
            }
        }

        public T this[int index]
        {
            get
            {
                var current = Root;
                var lastIndex = 0;
                for (; ; )
                {
                    if (current == null) 
                        throw new IndexOutOfRangeException();
                    var currentIndex = (current.Left == null ? 0 :
                        current.Left.Weight + 1) + lastIndex;
                    if (currentIndex == index) return current.Value;
                    if (currentIndex < index)
                    {
                        current = current.Right;
                        lastIndex = currentIndex + 1;
                    }
                    else current = current.Left;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
