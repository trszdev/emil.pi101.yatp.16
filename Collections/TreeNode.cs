﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class TreeNode<T>
    {
        public TreeNode<T> Left { get; set; }
        public int Weight { get; set; }
        public TreeNode<T> Right { get; set; }
        public T Value { get; private set; }

        public TreeNode(T value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
