﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collections;

namespace CollectionsExample
{
    static class Program
    {
        static void Main(string[] args)
        {
            var tree = new BinaryTree<int>(new[] { 1, 2, 3, 3, 3, 3, 3, 1, 1, 2, 0 });
            var expected = new[] { 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3 };
            for (var i = 0; i < expected.Length; i++)
            {
                Console.WriteLine(tree[i]);
            }
                
        }
    }
}
